﻿using SMC.Data.Models;
using SMC.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMC.Data.Context;

namespace SMC.Data.Repositories
{
    public class VendingMachineRepository : GenericRepository<VendingMachine, Guid>, IVendingMachineRepository
    {
        public VendingMachineRepository(DbBusinessContext dbContext) : base(dbContext)
        {
        }
    }
}
