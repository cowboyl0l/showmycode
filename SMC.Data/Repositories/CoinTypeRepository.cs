﻿using SMC.Data.Context;
using SMC.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMC.Data.Repositories.Interfaces
{
    public class CoinTypeRepository : GenericRepository<CoinType, Guid>, ICoinTypeRepository
    {
        public CoinTypeRepository(DbBusinessContext Context) : base(Context)
        {
        }
    }
}
