﻿using SMC.Data.Models;
using SMC.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMC.Data.Context;

namespace SMC.Data.Repositories
{
    public class ProductRepository : GenericRepository<Product, Guid>, IProductRepository
    {
        public ProductRepository(DbBusinessContext Context) : base(Context)
        {
        }
    }
}
