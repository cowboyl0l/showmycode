﻿using SMC.Data.Context;
using SMC.Data.Models;
using SMC.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMC.Data.Repositories
{
    public class MoneyRepository : GenericRepository<Money, Guid>, IMoneyRepository
    {
        public MoneyRepository(DbBusinessContext Context) : base(Context)
        {
        }
    }
}
