﻿using SMC.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SMC.Data.Context
{
    public class DbBusinessContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<CoinType> CoinTypes { get; set; }
        public DbSet<Money> Money { get; set; }
        public DbSet<VendingMachine> VendingMachine { get; set; }

        static DbBusinessContext()
        {
            Database.SetInitializer<DbBusinessContext>(new MyContextInitializer());
        }

        public DbBusinessContext() : base("ConnectionString")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CoinType>().ToTable("CoinTypes");
            modelBuilder.Entity<Money>().ToTable("Money");
            modelBuilder.Entity<Product>().ToTable("Products");
            modelBuilder.Entity<VendingMachine>().ToTable("VendingMachines");

            base.OnModelCreating(modelBuilder);
        }
    }

    class MyContextInitializer : CreateDatabaseIfNotExists<DbBusinessContext>
    {
        protected override void Seed(DbBusinessContext db)
        {
            var vm = new VendingMachine { CurrentBalance = 0 };

            var coin10rub = new CoinType { Value = 10, VendingMachine = vm };
            var coin5rub = new CoinType { Value = 5, VendingMachine = vm };
            var coin2rub = new CoinType { Value = 2, VendingMachine = vm };
            var coin1rub = new CoinType { Value = 1, VendingMachine = vm };

            db.CoinTypes.Add(coin10rub);
            db.CoinTypes.Add(coin5rub);
            db.CoinTypes.Add(coin2rub);
            db.CoinTypes.Add(coin1rub);

            db.Products.Add(new Product { Name = "Чай", Cost = 13, InStock = 10, VendingMachine = vm });
            db.Products.Add(new Product { Name = "Кофе", Cost = 18, InStock = 20, VendingMachine = vm });
            db.Products.Add(new Product { Name = "Кофе с молоком", Cost = 21, InStock = 20, VendingMachine = vm });
            db.Products.Add(new Product { Name = "Сок", Cost = 35, InStock = 15, VendingMachine = vm });

            // Vm Wallet
            db.Money.Add(new Money { CoinType = coin10rub, Count = 100, MoneyType = MoneyType.VmWallet, VendingMachine = vm });
            db.Money.Add(new Money { CoinType = coin5rub, Count = 100, MoneyType = MoneyType.VmWallet, VendingMachine = vm });
            db.Money.Add(new Money { CoinType = coin2rub, Count = 100, MoneyType = MoneyType.VmWallet, VendingMachine = vm });
            db.Money.Add(new Money { CoinType = coin1rub, Count = 100, MoneyType = MoneyType.VmWallet, VendingMachine = vm });

            // Customer Wallet 
            db.Money.Add(new Money { CoinType = coin10rub, Count = 15, MoneyType = MoneyType.CustomerWallet, VendingMachine = vm });
            db.Money.Add(new Money { CoinType = coin5rub, Count = 20, MoneyType = MoneyType.CustomerWallet, VendingMachine = vm });
            db.Money.Add(new Money { CoinType = coin2rub, Count = 30, MoneyType = MoneyType.CustomerWallet, VendingMachine = vm });
            db.Money.Add(new Money { CoinType = coin1rub, Count = 10, MoneyType = MoneyType.CustomerWallet, VendingMachine = vm });

            

            db.SaveChanges();
        }
    }





}
