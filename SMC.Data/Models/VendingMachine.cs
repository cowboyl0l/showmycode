﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMC.Data.Models
{
    public class VendingMachine
    {
        public VendingMachine()
        {
            this.Money = new HashSet<Money>();
            this.Products = new HashSet<Product>();
            this.CoinTypes = new HashSet<CoinType>();
        }

        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public int CurrentBalance { get; set; }


        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<Money> Money { get; set; }
        public virtual ICollection<CoinType> CoinTypes { get; set; }
    }
}
