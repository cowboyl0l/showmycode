﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMC.Data.Models
{
    public class CoinType
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public int Value { get; set; }

        public Nullable<Guid> VendingMachineId { get; set; }

        [ForeignKey("VendingMachineId")]
        public virtual VendingMachine VendingMachine { get; set; }
    }
}
