﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMC.Data.Models
{
    public class Product
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Cost { get; set; }
        public int InStock { get; set; }
        public Nullable<Guid> VendingMachineId { get; set; }

        [ForeignKey("VendingMachineId")]
        public virtual VendingMachine VendingMachine { get; set; }
    }
}
