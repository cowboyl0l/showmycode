﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMC.Data.Models
{
    public class Money
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid CoinTypeId { get; set; }
        public Nullable<Guid> VendingMachineId { get; set; }
        public int Count { get; set; }
        public MoneyType MoneyType { get; set; }

        [ForeignKey("CoinTypeId")]
        public virtual CoinType CoinType { get; set; }
        [ForeignKey("VendingMachineId")]
        public virtual VendingMachine VendingMachine { get; set; }

    }

    public enum MoneyType
    {
        CustomerWallet,
        VmWallet
    }
}
