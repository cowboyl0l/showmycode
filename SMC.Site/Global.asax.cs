﻿using Autofac;
using Autofac.Integration.Mvc;
using SMC.Data.Context;
using SMC.Data.Repositories;
using SMC.Data.Repositories.Interfaces;
using SMC.VmService;
using SMC.VmService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Compilation;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Inspinia_MVC5
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {

            var builder = new ContainerBuilder();

            // REGISTER DEPENDENCIES
            builder.RegisterType<DbBusinessContext>().AsSelf().InstancePerRequest();

            //CUSTOM DI
            builder.RegisterType(typeof(CoinTypeRepository)).As(typeof(ICoinTypeRepository)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(ProductRepository)).As(typeof(IProductRepository)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(MoneyRepository)).As(typeof(IMoneyRepository)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(VendingMachineRepository)).As(typeof(IVendingMachineRepository)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(VmService)).As(typeof(IVmService)).InstancePerLifetimeScope();

            var assemblies = BuildManager.GetReferencedAssemblies().Cast<Assembly>().ToArray();
            // REGISTER CONTROLLERS SO DEPENDENCIES ARE CONSTRUCTOR INJECTED
            builder.RegisterControllers(typeof(MvcApplication).Assembly);




            // BUILD THE CONTAINER
            var container = builder.Build();

            // REPLACE THE MVC DEPENDENCY RESOLVER WITH AUTOFAC
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));




            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
