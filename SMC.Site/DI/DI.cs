﻿using Autofac;
using SMC.Data.Repositories;
using SMC.Data.Repositories.Interfaces;
using SMC.VmService;
using SMC.VmService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inspinia_MVC5.DI
{
    public class DI : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType(typeof(CoinTypeRepository))
                .As(typeof(ICoinTypeRepository))
                 .InstancePerMatchingLifetimeScope("work");

            builder.RegisterType(typeof(MoneyRepository))
                .As(typeof(IMoneyRepository))
                 .InstancePerMatchingLifetimeScope("work");

            builder.RegisterType(typeof(ProductRepository))
                .As(typeof(IProductRepository))
                 .InstancePerMatchingLifetimeScope("work");

            builder.RegisterType(typeof(VmService))
                .As(typeof(IVmService))
                 .InstancePerMatchingLifetimeScope("work");

            builder.RegisterType(typeof(VendingMachineRepository))
                .As(typeof(IVendingMachineRepository))
                 .InstancePerMatchingLifetimeScope("work");
        }
    }
}