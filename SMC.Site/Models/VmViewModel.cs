﻿using SMC.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inspinia_MVC5.Models
{
    public class VmViewModel
    {
        public IEnumerable<Product> Products { get; set; }
        public IEnumerable<Money> CustomerMoney { get; set; }
        public IEnumerable<Money> VmMoney { get; set; }
        public int currentBalance { get; set; }
    }
}