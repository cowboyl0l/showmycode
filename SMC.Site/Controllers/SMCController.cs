﻿using Inspinia_MVC5.Models;
using SMC.Data.Repositories.Interfaces;
using SMC.VmService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Inspinia_MVC5.Controllers
{
    public class SMCController : Controller
    {
        protected readonly IVmService _vmService;


        public SMCController(IVmService vmService)
        {
            _vmService = vmService;
        }


        public ActionResult Index()
        {
            var customerMoney = _vmService.GetCustomerMoney();
            var VmMoney = _vmService.GetVMMoney();
            var products = _vmService.GetAllProducts();
            var currentBalance = _vmService.GetVendingMachine().CurrentBalance;
            var model = new VmViewModel { CustomerMoney = customerMoney, Products = products, VmMoney = VmMoney, currentBalance = currentBalance };
            return View(model);
        }


        public ActionResult AddCoin(Guid id)
        {
           var moneyAdd = _vmService.AddCoin(id);
           return Content("Добавлено " + moneyAdd + " руб.");
        }

        public ActionResult BuyProduct(Guid id)
        {
            var response = _vmService.BuyProduct(id);
            return Content(response);
        }

        public ActionResult GetChange()
        {
            _vmService.GetChange();
            return Content("Заберите сдачу");
        }

    }
}