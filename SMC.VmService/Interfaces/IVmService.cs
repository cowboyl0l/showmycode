﻿using SMC.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMC.VmService.Interfaces
{
    public interface IVmService
    {
        IEnumerable<Product> GetAllProducts();
        IEnumerable<Money> GetCustomerMoney();
        IEnumerable<Money> GetVMMoney();
        VendingMachine GetVendingMachine();
        int AddCoin(Guid coinTypeId);
        string BuyProduct(Guid productId);
        void GetChange();
    }
}
