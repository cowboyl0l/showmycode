﻿using SMC.Data.Models;
using SMC.Data.Repositories.Interfaces;
using SMC.VmService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SMC.VmService
{
    public class VmService : IVmService
    {
        protected readonly ICoinTypeRepository _coinTypeRepository;
        protected readonly IProductRepository _productRepository;
        protected readonly IMoneyRepository _moneyRepository;
        protected readonly IVendingMachineRepository _vendingMachineRepository;

        public VmService(ICoinTypeRepository coinTypeRepository,
            IProductRepository productRepository,
            IMoneyRepository moneyRepository,
            IVendingMachineRepository vendingMachineRepository
            )
        {
            _vendingMachineRepository = vendingMachineRepository;
            _coinTypeRepository = coinTypeRepository;
            _productRepository = productRepository;
            _moneyRepository = moneyRepository;
        }

        /// <summary>
        ///  Все продукты вендингового автомата
        /// </summary>
        public IEnumerable<Product> GetAllProducts()
        {
            return _productRepository.SelectAll();
        }

        /// <summary>
        ///  Деньги покупателя в кошельке
        /// </summary>
        public IEnumerable<Money> GetCustomerMoney()
        {
            return _moneyRepository.FindAll(p => p.MoneyType == (MoneyType.CustomerWallet));
        }

        /// <summary>
        ///  Деньги автомата
        /// </summary>
        public IEnumerable<Money> GetVMMoney()
        {
            return _moneyRepository.FindAll(p => p.MoneyType == (MoneyType.VmWallet));
        }
        /// <summary>
        ///  получение вендингового аппарата
        /// </summary>
        public VendingMachine GetVendingMachine()
        {
            return _vendingMachineRepository.SelectAll().FirstOrDefault();
        }

        /// <summary>
        ///  Добавление монеты в автомат
        /// </summary>
        public int AddCoin(Guid coinTypeId)
        {
            using (var tr = new TransactionScope())
            {
                var coin = _coinTypeRepository.SelectByID(coinTypeId);
                var customerMoney = _moneyRepository.Find(p => p.CoinTypeId == (coinTypeId) && p.MoneyType == MoneyType.CustomerWallet);
                // if customer have this coin
                if (customerMoney.Count > 0)
                {
                    try
                    {
                        var VmMoney = _moneyRepository.Find(p => p.CoinTypeId == (coinTypeId) && p.MoneyType == MoneyType.VmWallet);
                        VmMoney.Count++;
                        customerMoney.Count--;
                        var vm = GetVendingMachine();
                        vm.CurrentBalance += coin.Value;
                        _vendingMachineRepository.Update(vm);
                        _moneyRepository.Update(VmMoney);
                        _moneyRepository.Update(customerMoney);
                        tr.Complete();
                        return coin.Value;
                    }
                    catch
                    {

                    }
                }
            }
            return 0;
        }

        public string BuyProduct(Guid productId)
        {
            using (var tr = new TransactionScope())
            {
                var product = _productRepository.SelectByID(productId);
                var vm = GetVendingMachine();
                if (product.Cost <= vm.CurrentBalance)
                {
                    var haveCoins = IsHaveChange(vm.CurrentBalance - product.Cost);
                    if (haveCoins)
                    {
                        vm.CurrentBalance -= product.Cost;
                        product.InStock--;
                        _vendingMachineRepository.Update(vm);
                        _productRepository.Update(product);
                        tr.Complete();
                        return "Вы успешно купили продукт";
                    }
                    else
                    {
                        return "В автомате нет необходимых монет, для сдачи";
                    }
                }
                
            }
            return "Пополните баланс, не хватает средств";
        }

        /// <summary>
        ///  выдача сдачи  
        /// </summary>
        public void GetChange()
        {
            using (var tr = new TransactionScope())
            {
                try
                {
                    var vm = GetVendingMachine();
                    var avilibleCoins = _moneyRepository.FindAll
                    (p => p.MoneyType == MoneyType.VmWallet)
                    .ToDictionary(p => p.CoinType.Value);

                    foreach (var coins in avilibleCoins)
                    {
                        if (coins.Value.Count > 0)
                        {
                            if (vm.CurrentBalance >= coins.Key)
                            {
                                var howMuchCoins = vm.CurrentBalance / coins.Key;
                                // если необходимо выдать например 5 моент по 10, но есть только 2, отдаем 2.
                                if(howMuchCoins > coins.Value.Count)
                                {
                                    howMuchCoins = coins.Value.Count;
                                }
                                vm.CurrentBalance -= howMuchCoins * coins.Key;
                                var customerMoney = _moneyRepository.Find(p => p.CoinType.Value == coins.Key && p.MoneyType == MoneyType.CustomerWallet);
                                customerMoney.Count += howMuchCoins;
                                _moneyRepository.Update(customerMoney);
                                coins.Value.Count -= howMuchCoins;
                                _moneyRepository.Update(coins.Value);
                                _vendingMachineRepository.Update(vm);
                            }
                        }
                    }
                    tr.Complete();
                }
                catch
                {
                   
                }
            }
        }


        /// <summary>
        ///  Будет ли сдача при заданном балансе?
        /// </summary>
        private bool IsHaveChange(int balance)
        {
            var test = _moneyRepository.FindAll(p => p.MoneyType == MoneyType.VmWallet);
            var avilibleCoins = _moneyRepository.FindAll
                    (p => p.MoneyType == MoneyType.VmWallet)
                    .ToDictionary(p => p.CoinType.Value);
            foreach (var coins in avilibleCoins)
            {
                if (coins.Value.Count > 0)
                {
                    if (balance >= coins.Key)
                    {
                        var howMuchCoins = balance / coins.Key;
                        if (howMuchCoins > coins.Value.Count)
                        {
                            howMuchCoins = coins.Value.Count;
                        }
                        coins.Value.Count -= howMuchCoins;
                        balance -= howMuchCoins * coins.Key;
                    }
                }
            }
            return balance == 0;
        }
    }
}